using DoubleQuack
import SymNLPModels as NLP
using Symbolics
using Base.Iterators: take
using NLPModelsIpopt: ipopt
using Printf
using Symbolics: build_function, get_variables

# HELPER FUNCTIONS
function clean_print_strat(pures::Vector{<:NTuple{N}}, probs) where N
    function centeri(j)
        total = sum(probs[i] * pures[i][j] for i in eachindex(probs))
        round(total; digits=3)
    end

    println("Centered at ", ntuple(centeri, N), ":")

    perm = sortperm(probs; rev=true)

    for i in perm
        if probs[i] < 5e-4
            break
        end

        @printf "%10.1f%% %s\n" probs[i]*100 round.(pures[i], digits=3)
    end
end

function clean_print(puress, probss)
    print("Player 1 - ")
    clean_print_strat(puress[1], probss[1])
    println()
    print("Player 2 - ")
    clean_print_strat(puress[2], probss[2])
end

function print_exploitability(exploit)
    println("Exploitability sequence")
    for i in eachindex(exploit)
        @printf "%5.3f\n" exploit[i]
    end
end

function run_experiment_solve(p, ds, vs; iterations)
    quack = DoubleQuack.quack_oracle(p, ds, vs)
    pure, prob, _ = DoubleQuack.fixed_iters(quack, iterations)

    pure, prob
end

function run_experiment_exploit(p, ds, vs; iterations)
    quack = DoubleQuack.quack_oracle(p, ds, vs)
    exploit = Array{Float64}(undef, iterations)
    iter = 1
    for (actions, probs, values, best) in take(quack, iterations)
        exploit[iter] = sum(best)
        iter += 1
        clean_print(actions, probs)
    end

    exploit
end

function best_response_function(payoff_partial, pures, weights)
    total = 0.0
    for (pure, weight) in zip(pures, weights)
        total += weight * payoff_partial(pure)
    end
    Symbolics.simplify(total; expand=true)
end

function best_response_functions(payoff, pures, weights, variables)
    p_partial1(pure) = +payoff(variables[1], pure)
    p_partial2(pure) = -payoff(pure, variables[2])
    p1 = best_response_function(p_partial1, pures[2], weights[2])
    p2 = best_response_function(p_partial2, pures[1], weights[1])

    p1, p2
end

function oracle1(payoff, domains, actions, weights, variables)
    unilateral = best_response_functions(payoff, actions, weights, variables)
    display("Player 1")
    or1 = oracle(unilateral[1], domains[1], variables[1])
    max1 = or1[1]
    display(max1)
    display(or1[2])
    or = oracle(-unilateral[1],domains[1],variables[1])
    min1 = -or[1]
    display(min1)
    display(or[2])
    display("RANGE 1")
    range1 = max1-min1
    display(range1)

    display("Player 2")
    or2 = oracle(unilateral[2],domains[2],variables[2])
    max2 = or2[1]
    display(max2)
    display(or2[2])
    or = oracle(-unilateral[2],domains[2],variables[2])
    min2 = -or[1]
    display(min2)
    display(or[2])
    display("RANGE 2")
    range2 = max2-min2
    display(range2)

    improved = [or1,or2]
    vals, args = unzip(improved)
    Tuple(vals), Tuple(args), max1, max2, range1, range2
end

domains_variables(domains) = map(player_variables, domains)

unzip(a) = map(x -> getfield.(a, x), fieldnames(eltype(a)))

tuplecat(as...) = vcat(collect.(as)...)

player_variables(domain) = Tuple(unique(vcat(Symbolics.get_variables.(domain)...)))

function _compile_sym(sym, vars=[get_variables(sym)])
    build_function(sym, vars...; expression=false)
end

function exploit_stoner(p, ds, vs, sol)
    variables = domains_variables(vs)
    callable = _compile_sym(p, variables)
    or = oracle1(callable, ds, sol, ([1.0],[1.0]),variables)
    exploit = sum(or[1])
    exploit, or[2], or[3], or[4], or[5], or[6]
end

# STON'R EXPLOITABILITY COMPUTATION
n = 2
x = [Symbolics.variable(:x,i) for i in 1:n]
f = 0.2*x[1]*x[2]-cos(x[2])
doms = ((x[1] ≲ 1, -1 ≲ x[1],), (x[2] ≲ 2*π, -2*π ≲ x[2],))
vars = ((x[1],), (x[2],))

expl,args,max1,max2,rng1,rng2 = exploit_stoner(f, doms, vars, ([(0)], [(0)]))
sol = [-1,-1]
fn = eval(build_function(f,x))
norm_expl1 = (max1-(-fn(sol)))/rng1
norm_expl2 = (max2-fn(sol))/rng2
display(norm_expl1+norm_expl2)
display(expl)

# DOUBLE ORACLE EXPLOITABILITY COMPUTATION
pure,prob = run_experiment_solve(f, doms, vars; iterations=10)
clean_print(pure,prob)
es = run_experiment_exploit(-f, doms, vars; iterations=10)